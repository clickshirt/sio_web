define('csinit', [
	'jquery',
	'handlebars'
	], function($, Handlebars) {
		var templates = window.templates = {};
		var SPToken = window.SPToken = "f842fa58fdbb40e8af3846a098e0f38b";
		var globalApi = window.globalApi = "https://:" + SPToken + "@api.scalablepress.com/v2";
		var postApi = window.postApi = "http://shirts-api.openrobot.net";
		$('#templates').children('div').each(function () {
			templates[$(this).attr('id')] = Handlebars.compile($(this).html());
		});
		var ret = {
			default: {
				sample_style: {
					top: 90,
					left: 150,
					width: 240,
					height: 380
				},
				image_style: {
					width: 540
				},
				container_style: {
					width: 425,
					height: 600
				}
			},
			getParameterByName: function (name) {
			    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			    results = regex.exec(location.search);
			    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
			}
		}
		return ret;
	}
);