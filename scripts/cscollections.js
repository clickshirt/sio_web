define('cscollections', [
	'jquery',
	'csinit',
	'epoxy',
	'backbone',
	'csmodels'], function($, csinit, Epoxy, Backbone, CSModels) {
	var BaseCollection, CSCollections = {};
	CSCollections.BaseCollection = BaseCollection = Backbone.Collection.extend({
		model: CSModels.BaseModel,
		initialize: function(options) {

			BaseCollection.__super__.initialize.apply(this, arguments);
		}
	});
	CSCollections.Catagories = BaseCollection.extend({
		model: CSModels.Category,
		comparator: 'family',

		url: function() {
			return globalApi + "/categories"
		}
	});

	CSCollections.Designs = BaseCollection.extend({
		model: CSModels.Design,
		comparator: 'createdAt',

		url: function() {
			return globalApi + ""
		}
	});

	CSCollections.Products = BaseCollection.extend({
		model: CSModels.Product,
		comparator: 'available',

		url: function() {
			return globalApi + "/products/"
		}
	});

	CSCollections.OrderCollection = BaseCollection.extend({
		model: CSModels.OrderResponse,
		url: function() {
			return globalApi + "/order"
		}
	});
	return CSCollections;
});
