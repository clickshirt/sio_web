define([
	'jquery',
	'underscore',
	'backbone',
	'epoxy',
	'csinit',
	'csfabric',
	'csmodels',
	'csviews/BaseView'
	], function($, _, Backbone, Epoxy, csinit, CSFabric, CSModels, BaseView) {
	var CanvasEditor = BaseView.extend({
		events: {
			'change .rmWhite': 'removeWhite'
		},

		className: 'clearfix',

		render: function() {
			this.$el.html(templates.CanvasEditor());
			this.$canvas = this.$el.find('.cseditor');
			this.showPreview();
			CanvasEditor.__super__.render.apply(this, arguments);
			return this;
		},

		initialize: function(options) {
			this.viewModel = new CSModels.BaseModel({
				imageData: "",
				artImageStyle: "",
				shirtImage: "",
				shirtImageStyle: "position:static; width:100%",
				containerStyle: "min-height:350px; width:325px; background-color:#ccc; border:1px solid #bbb; border-radius:3px; padding:3px;"
			});
			CanvasEditor.__super__.initialize.apply(this, arguments);
		},
		removeWhite: function(e) {
			$check = $(e.currentTarget);
			if (this.sampleImage.filters.length) {
				this.sampleImage.filters.pop();
			} else {
				this.sampleImage.filters.push(new CSFabric.Image.filters.RemoveWhite());
			}
			this.sampleImage.applyFilters(this.fCanvas.renderAll.bind(this.fCanvas));
		},

		showPreview: function() {
			var selectedProd = this.model.toJSON();
			var json = csinit;

			// use product specific options, or default
			var options;
			if (json[selectedProd.productId]) {
				options = json[selectedProd.productId];
			} else {
				options = json.default;	
			}
			this.fCanvas = new CSFabric.Canvas('cseditor');
			this.fCanvas.setHeight(550);
			this.fCanvas.setWidth(540);
			//this.fCanvas.selection = false;

			//select our best option image
			var productImage;
			var productImages = selectedProd.selectedColor.images
			for (var i = 0; i < productImages.length; i ++) {
				if (productImages[i].label === "Front") {
					productImage = productImages[i].url;
				}
			}
			productImage = productImage || selectedProd.productImage;

			//load images in canvas, and apply filters on callback
			CSFabric.Image.fromURL(productImage, _.bind(function(oImg) {
				oImg.set(options.image_style);
				oImg.set('selectable', false);
				this.fCanvas.add(oImg);
			}, this));
			CSFabric.Image.fromURL("/proxy.php?url="+this.model.get('artwork'), _.bind(function(oImg) {
				oImg.set(options.sample_style);
				oImg.filters.push(new CSFabric.Image.filters.RemoveWhite());
				oImg.applyFilters(this.fCanvas.renderAll.bind(this.fCanvas));
				this.sampleImage = oImg;
				this.fCanvas.add(oImg).renderAll();
    			this.fCanvas.setActiveObject(oImg);
				this.trigger('done_loading');
			}, this));
		},
		adjustImage: function(iArray) {asdf
		    var imageData = iArray.data;
		    for (var i = 0; i < imageData.length; i+= 4) {
		    	var dr = Math.abs(255 - imageData[i]);
				var dg = Math.abs(255 - imageData[i+1]);
				var db = Math.abs(255 - imageData[i+2]);
				//imageData[i] = 0;
				//imageData[i+1] = 0;
				//imageData[i+2] = 0;
		       	imageData[i+3] = 255 * ((dr + dg + db)/765);
		    }
		
		    return iArray;            
		},
	}); 
	return CanvasEditor;
});