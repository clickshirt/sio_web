define([
	'jquery',
	'underscore',
	'backbone',
	'epoxy'
	], function($, _, Backbone, Epoxy) {
	var BaseView = Backbone.Epoxy.View.extend({
		initialize: function(options) {
			this.options = options || {};
			BaseView.__super__.initialize.apply(this);
		},

		render: function() {
			this.applyBindings();
			this.delegateEvents();
			BaseView.__super__.render.apply(this);
		}

	});
	return BaseView;
});