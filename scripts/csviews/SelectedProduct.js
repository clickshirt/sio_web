define([
	'jquery',
	'underscore',
	'backbone',
	'epoxy',
	'csviews/BaseView',
	'csviews/AddressForm',
	'csviews/CanvasEditor',
	'csmodels'
	], function($, _, Backbone, Epoxy, BaseView, AddressForm, CanvasEditor, CSModels) {
	var SelectedProduct = BaseView.extend({
		className: 'selectedProduct',
		bindings: {
			'.size-select': 'options:sizes',
			'.artwork-url input': 'value:artwork',
			'.qty-select': 'options:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]'
		},
		events: {
			'click .close': 'closeButton',
			'click .fa-circle': 'pickColor',
			'change .size-select': 'sizeSelect',
			'change .qty-select': 'qtySelect',
			'change .artwork-url input': 'showPreview'
		},

		initialize: function(options) {
			this.shipping = options.shipping;
			this.default = true;
			SelectedProduct.__super__.initialize.apply(this, arguments);
		},
		
		render: function() {
			var productImage;
			var productImages = this.model.get('selectedColor').images;
			for (var i = 0; i < productImages.length; i ++) {
				if (productImages[i].label === "Front") {
					productImage = productImages[i].url;
				}
			}
			productImage = productImage || selectedProd.productImage;
			this.$el.html(templates.SelectedProductView({
				product: this.model.toJSON({
					computed: true
				})
			}));
			this.$addressForm = this.$el.find('.address-form');
			this.$canvas = this.$el.find('.img-preview');
			this.$el.show();
			this.buildSubviews();
			SelectedProduct.__super__.render.apply(this, arguments);
			if (this.default) {
				this.$el.find('.qty-select option').filter(function() { 
				    return ($(this).text() == '1');
				}).prop('selected', true);
				this.$el.find('.size-select option').filter(function() { 
				    return ($(this).text() == 'xlg');
				}).prop('selected', true);
				//this.default = false;
			};
		},
		buildSubviews: function() {
				this.addressWiget = new AddressForm({
					model: this.shipping,
					el: this.$addressForm
				});
				this.addressWiget.render();
				this.canvasEditor = new CanvasEditor({
					model: this.model,
					el: this.$canvas
				});
				this.canvasEditor.render();
		},
		showPreview: function() {
			this.canvasEditor.showPreview();
		},
		pickColor: function(e) {
			var product = this.model.toJSON();
			var color = this.model.get('colors')[$(e.currentTarget).data('color')];
			this.collection.trigger('option_selected', _.extend(product, {
					selectedColor: color,
					color: color.name,
					sizes: color.sizes
				})
			);
			$('html, body').animate({
			   scrollTop: $('.product-selection').offset().top
			}, 'slow');
		},
		sizeSelect: function(e) {
			var size = $(e.currentTarget).val();
			this.model.set('size', size);
		},
		qtySelect: function(e) {
			var qty = $(e.currentTarget).val();
			this.model.set('quantity', qty);
		}
	});
	return SelectedProduct;
});