define([
	'jquery',
	'underscore',
	'backbone',
	'epoxy',
	'csinit',
	'csviews/BaseView',
	'csviews/SelectedProduct',
	'csviews/ArtworkLanding',
	'csmodels'
	], function($, _, Backbone, Epoxy, csinit, BaseView, SelectedProduct, ArtworkLanding, CSModels) {
	var Shop = BaseView.extend({
		events: {
			'click .btn-save': 'handleQuote'
		},
		initialize: function(options) {
			this.options = options || {};
			this.categories = this.options.categories;
			this.products = this.options.products;
			this.shipping = this.options.shipping;
			this.prodSelection = this.options.prodSelected;
			this.viewModel = new CSModels.BaseModel({
				img:""
			});
			Shop.__super__.initialize.apply(this. arguments);
		},

		render: function() {
			this.$el.html(templates.App());
			this.$catagorySelect = this.$el.find('.category-pills');
			this.$images = this.$el.find('.image-container');
			this.$selection = this.$el.find('.product-selection');
			this.$artwork = this.$el.find('.artwork-landing');
			this.$page2 = this.$el.find('.page2');
			this.$footer = this.$el.find('.footer');
			this.buildSubviews();
			Shop.__super__.render.apply(this, arguments);
		},

		buildSubviews: function (){
			this.productSelection = new SelectedProduct({
				collection: this.products,
				el: this.$selection,
				model: this.prodSelection,
				shipping: this.shipping
			});
			this.$page2.hide();
			this.categories.on('option_selected', this.optionSelected, this);
			this.products.on('option_selected', this.productSelected, this);
			this.on('sendQuote', this.sendQuote, this);

			this.artworkLanding = new ArtworkLanding({
				el: this.$artwork,
				model: this.prodSelection
			});
			this.artworkLanding.on('artwork_input', this.artworkSelected, this);
			this.artworkLanding.render();
		},

		artworkSelected: function(e) {
			var product = new this.products.model({
				id: 'gildan-ultra-cotton-t-shirt'
			});
			product.fetch({
				success: _.bind(function(){
					var colors = product.get('colors');
					var queryColor = csinit.getParameterByName('color') || 'White';
					var chosen = 0;
					for (var i in colors) {
						if (colors[i].name == queryColor) {
							chosen = i;
						}
					}
					this.productSelected(_.extend(product.toJSON(), {
							selectedColor: product.get('colors')[chosen],
							color: queryColor,
							sizes: product.get('colors')[chosen].sizes,
							artwork: e
						})
					);
					$('html, body').animate({
					   scrollTop: $('.product-selection').offset().top
					}, 'slow');
					this.productSelection.canvasEditor.on('done_loading', _.bind(function() {
						this.$page2.show();
						this.$artwork.hide();
					}, this));
					this.productSelection.canvasEditor.showPreview();
				}, this)
			});
		},

		optionSelected: function(e) {
			this.catSelection = this.categories.get(e);
			this.catSelection.fetch({
				success: _.bind(this.gotProducts, this)
			});
		},

		gotProducts: function() {
			this.products.reset(this.catSelection.get('products'));
			this.products.trigger('update');
		},

		productSelected: function(e) {
			this.prodSelection.set(e);
			this.productSelection.render();
			this.prodSelection.on("change", this.calcSavable, this);
			this.shipping.on("parsed", this.calcSavable, this);
		},
		handleQuote: function() {
			var order = _.extend({}, _.pick(this.prodSelection.toJSON(), 
				['productId',
				'color',
				'size',
				'quantity',
				'selectedColor']));
			order = _.extend({}, order, _.pick(this.shipping.toJSON(),
				['address1',
				 'input',
				 'country',
				 'zip',
				 'state', 
				 'city']));
			order.type = 'dtg';
			order.sides = {front:1};
			order.placeOrder = 1;
			order.width = 12;
			//order.artwork = "http://google.com/default.png";
			order.productImage = this.prodSelection.get('image').url;
			order.fromUrl = true;

			this.quote = _.extend({}, order);
			var handler = StripeCheckout.configure({
				key: 'pk_live_P9WcuSbQ9Z6ChFCDEnFZ6dJH',
				image: order.artwork,
				closed: _.bind(function(e) {
				}, this),
				token: _.bind(function(token) {
					if (token.email) {
						$.ajax({
							type: 'POST',
							data: _.extend(order, token),
							url: "http://shirts-api.openrobot.net",
							success: _.bind(function(data){
								console.log(data);
								
							}, this),
							error: _.bind(function(data){
								alert("Error, please contact xanderjanz@gmail.com");
							}, this)
						});
					}
				}, this)
			});
			// Open Checkout with further options
			handler.open({
				name: 'ClickShirt',
				description: order.quantity + " " +
							 order.color + 
							 ' printed product(s). ($20.00 ea)',
				amount: 2000 * order.quantity
			});
		},
		calcSavable: function(e) {
			var shipping = this.shipping.toJSON();
			var product = this.prodSelection.toJSON()
			var productTests = 
				['productId',
				'color',
				'size',
				'quantity',
				'selectedColor'];
			var shippingTests = 
				['address1',
				 'country',
				 'zip',
				 'state', 
				 'city'] 
			var valid = true;
			for (var test in productTests) {
				valid = valid && !!(product[productTests[test]]);
			}
			for (var test in shippingTests) {
				valid = valid && !!(shipping[shippingTests[test]]);
			}
			if (valid) {
				this.$el.find('.btn-save').removeClass("disabled");
			} else {
				this.$el.find('.btn-save').addClass("disabled");
			}
		},
		sendQuote: function(link) {
			var link = link.data;
			if (link.srcUrl) {
				this.quote.Artwork = link.srcUrl;
				$.ajax({
					type: 'POST',
					data: this.quote,
					url: postApi,
					success: _.bind(function(data){
						alert(JSON.stringify(data));
					}, this)
				});
			}
		}
	});
	return Shop;
});