define([
	'jquery',
	'underscore',
	'backbone',
	'epoxy',
	'csviews/BaseView'
	], function($, _, Backbone, Epoxy, BaseView) {
	var AddressForm = BaseView.extend({
		events: {
			'keyup textarea': 'calcAddress'
		},
		bindings: {
			'.address-email input': 'value:email'
		},
		initialize: function(options){
			this.options = options || {};
			this.model = this.options.model;
			this.model.set('typing', '');
			AddressForm.__super__.initialize.apply(this, arguments);
			this.render();
		},
		calcAddress: function(e){
			var input = $(e.currentTarget).val();
			_.debounce(_.bind(this.requestAddress, this, input), 600)();
		},
		render: function() {
			this.$el.html(templates.address());
			AddressForm.__super__.render.apply(this, arguments);
			return this;
		},
		requestAddress: function(input){
			$.ajax( {
				url: "http://api.addresslabs.com/v1/parsed-address?address=" + encodeURIComponent(input.replace(/\n/g,"+").replace(/ /g, "+")),
				success: _.bind(function(data) {
					if (data.zip && data.street) {
						var email = this.model.get('email');
						this.model.clear();
						this.model.set(this.model.defaults);
						this.model.set('email', email);
						this.model.set('address1', data.delivery.address_line);
						this.model.set(data);
						this.model.trigger("parsed");
						this.$el.find('.form-group').removeClass('has-error');
					} else {
						this.$el.find('.form-group').addClass('has-error');
					}
				}, this),
				error: _.bind(function(data) {
					this.model.clear();
					this.model.set(this.model.defaults);
					this.$el.find('.form-group').addClass('has-error');
					this.model.trigger("parsed");
				}, this)
			});
		}
	});
	return AddressForm
});