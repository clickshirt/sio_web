define([
	'jquery',
	'underscore',
	'backbone',
	'epoxy',
	'csviews/BaseView'
	], function($, _, Backbone, Epoxy, BaseView) {
	var ArtworkLanding = BaseView.extend({
		events: {
			'change input': 'progressViews'
		},
		bindings: {
			'input': 'value:artwork'
		},
		render: function() {
			if (this.model.get('artwork').length > 0) {
				this.progressViews();
			} else {
				this.$el.html(templates.ArtworkLanding());
				ArtworkLanding.__super__.render.apply(this, arguments);
			}
			return this;
		},
		progressViews: function() {
			this.trigger('artwork_input', this.model.get('artwork'));
		}
	});
	return ArtworkLanding;
});