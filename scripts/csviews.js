define('csviews', [
	'jquery',
	'underscore',
	'csinit',
	'epoxy',
	'backbone',
	'csfabric',
	'csmodels',
	'cscollections'], function($, _, csinit, Epoxy, Backbone, CSFabric, CSModels, CSCollections) {
	Backbone.Epoxy.binding.addHandler('validate', {
		init: function ($boundEl, params, bindings, context) {
			this.validations = params.validations;

			for (var i = 0; i < this.validations.length; i++) {
				this.validations[i].id = _.uniqueId(this.validations[i].type);
			}

			this.$validationContainer = $boundEl.closest('.form-group');
		},

		get: function ($boundEl, value, evt) {

		},

		set: function ($boundEl, params) {
			for (var i = 0; i < this.validations.length; i++) {
				var type = this.validations[i].type;
				var validationParams = this.validations[i].params;
				var id = this.validations[i].id;

				var result = this['validate_' + type](params.value, validationParams);

				if (result === true) {
					this.clearFailedValidations(id);
				} else {
					var severity = 'danger';

					if (typeof result === 'object') {
						severity = result.severity;
						result = result.message;
					}
					this.clearFailedValidations(id);
					this.addFailedValidation(result, id, severity);
				}
			}
		},

		addFailedValidation: function (result, id, severity) {
			var existingError = this.getValidationError(id, severity);

			if (!existingError || !existingError.length) {
				if (severity === 'danger' || severity === 'error') {
					this.$validationContainer.addClass('has-error');
				}
			}
		},

		clearFailedValidations: function (id) {
			this.getValidationError(id).remove();

			// If there are no more validation messages, remove the error class
			if (0 === this.$validationContainer.find('.validation-message').length) {
				this.$validationContainer.removeClass('has-error');
			}
		},

		getValidationError: function (id, severity) {
			var query = '[data-id=' + id + ']';

			if (severity) {
				query += '[data-severity=' + severity + ']';
			}

			return this.$validationContainer.find(query);
		},

		validate_number: function (value, validationParams) {
			var result = true;

			if (value === null || value === undefined) {
				value = 0;
			}

			if (validationParams.max !== undefined && validationParams.max < value) {
				return 'Max ' + validationParams.max;
			}

			if (validationParams.min !== undefined && value < validationParams.min) {
				return 'Min ' + validationParams.min;
			}

			return result;
		},

		validate_length: function (value, validationParams) {
			var result = true;

			if (value === null || value === undefined) {
				value = '';
			}

			if (validationParams.max !== undefined) {
				if (validationParams.max < value.length) {
					return {
						message: 'Max length: ' + validationParams.max + ' (Currently: ' + value.length + ')',
						severity: validationParams.severity || 'error'
					};
				} else if (validationParams.min !== undefined && value.length < validationParams.min) {
					return {
						message: 'Min length: ' + validationParams.min + ' (Currently: ' + value.length + ')',
						severity: validationParams.severity || 'error'
					};
				} else {
					return {
						severity: 'info',
						message: 'Remaining: ' + (validationParams.max - value.length)
					};
				}
			}


			return result;
		},

		validate_regexMatch: function (value, validationParams) {
			var result = true;

			if (value === null || value === undefined) {
				value = '';
			}

			if (typeof validationParams.pass === 'string') {
				validationParams.pass = new RegExp(validationParams.pass, 'g');
			}

			if (typeof validationParams.fail === 'string') {
				validationParams.fail = new RegExp(validationParams.fail, 'g');
			}

			var passMatches = value.match(validationParams.pass);
			var failMatches = value.match(validationParams.fail);

			if (validationParams.pass && passMatches !== null && 0 === passMatches.length) {
				return validationParams.message || 'No matches for ' + validationParams.pass;
			} else if (validationParams.fail && failMatches !== null && 0 < failMatches.length) {
				return validationParams.message || 'Matched ' + validationParams.fail;
			}

			return result;
		}
	});
var BaseView, CSViews = {};
CSViews.BaseView = BaseView = Backbone.Epoxy.View.extend({
	initialize: function(options) {
		this.options = options || {};
		BaseView.__super__.initialize.apply(this);
	},

	render: function() {
		this.applyBindings();
		this.delegateEvents();
		BaseView.__super__.render.apply(this);
	}

});
CSViews.dropDown = BaseView.extend({
	events: {
		'click button': 'findCatagories',
		'change select': 'categoryPicked'
	},
	bindings: {
		'select': 'options:collection, optionsDefault:defaultOption',
		'h2': 'html:titleLabel'
	},
	initialize: function(options) {
		this.options = options || {};

		this.collection = this.options.collection;
		this.viewModel = new CSModels.BaseModel({
			collection: this.collection,
			defaultOption: this.options.defaultOption,
			titleLabel: this.options.titleLabel
		})
		CSViews.dropDown.__super__.initialize.apply(this, arguments);
	},

	render: function() {
		this.$el.html(templates.dropDown());

		CSViews.dropDown.__super__.render.apply(this, arguments);
		return this;
	},

	categoryPicked: function(e) {
		var value = $(e.currentTarget).val();
		this.trigger('option_selected', value);
		//this.on('change select', this.categoryPicked, this);
	}
});
CSViews.PillView = BaseView.extend({
	tagName: 'li',
	events: {
		'click a': 'pillClicked'
	},
	initialize: function(options) {
		CSViews.PillView.__super__.initialize.apply(this, arguments);
		this.render();
	},
	render: function() {
		this.$el.html(templates.Pill({
			model: this.model.toJSON({computed: true})
		}));
		CSViews.PillView.__super__.render.apply(this, arguments);
	},
	pillClicked: function(e) {
		this.$el.siblings().each(function () {
			$(this).removeClass('active');
		});
		this.$el.addClass('active');
		var value = $(e.currentTarget).val();
		this.model.collection.trigger('option_selected', this.model);
	}
});
CSViews.PillListView = BaseView.extend({
	itemView: CSViews.PillView,
	className:"nav nav-pills",
	render: function() {
		this.$el.html(templates.PillList());
		CSViews.PillListView.__super__.render.apply(this);
	}
})
CSViews.addressForm = BaseView.extend({
	events: {
		'keyup textarea': 'calcAddress'
	},
	bindings: {
		'.address-email input': 'value:email'
	},
	initialize: function(options){
		this.options = options || {};
		this.model = this.options.model;
		this.model.set('typing', '');
		CSViews.addressForm.__super__.initialize.apply(this, arguments);
		this.render();
	},
	calcAddress: function(e){
		var input = $(e.currentTarget).val();
		_.debounce(_.bind(this.requestAddress, this, input), 600)();
	},
	render: function() {
		this.$el.html(templates.address());
		CSViews.addressForm.__super__.render.apply(this, arguments);
		return this;
	},
	requestAddress: function(input){

			$.ajax( {
				url: "http://api.addresslabs.com/v1/parsed-address?address=" + encodeURIComponent(input.replace(/\n/g,"+").replace(/ /g, "+")),
				success: _.bind(function(data) {
					if (data.zip && data.street) {
						var email = this.model.get('email');
						this.model.clear();
						this.model.set(this.model.defaults);
						this.model.set('email', email);
						this.model.set('address1', data.delivery.address_line);
						this.model.set(data);
						this.model.trigger("parsed");
						this.$el.find('.form-group').removeClass('has-error');
					} else {
						this.$el.find('.form-group').addClass('has-error');
					}
				}, this),
				error: _.bind(function(data) {
					this.model.clear();
					this.model.set(this.model.defaults);
					this.$el.find('.form-group').addClass('has-error');
					this.model.trigger("parsed");
				}, this)
			});
		}

	});

	CSViews.address1Form = BaseView.extend({
		bindings: {
			'.address-email input': 'value:email',
			'.address-name input': 'value:name',
			'.address-company input': 'value:company',
			'.address-address1 input': 'value:address1',
			'.address-address2 input': 'value:address2',
			'.address-city input': 'value:city',
			'.address-state input': 'value:state',
			'.address-zip input': 'value:zip',
			'label': 'classes:{"col-xs-2": true, "control-label":true}',
			'div.address': 'classes:{"col-xs-10": true}',
			'div.address input': 'classes:{"form-control":true}'
		},
		validation: {
			'.address-email input': {
				value: 'email',
				validations: [{
					type: 'length',
					params: {
						min: 1,
						max:100
					}
				}]
			},
			'.address-name input': {
				value: 'name',
				validations: [{
					type: 'length',
					params: {
						min: 1,
						max:100,
						severity: 'error'
					}
				}]
			},
			'.address-address1 input': {
				value: 'address1',
				validations: [{
					type: 'length',
					params: {
						min: 5,
						max:100,
						severity: 'error'
					}
				}]
			},
			'.address-city input': {
				value: 'city',
				validations: [{
					type: 'length',
					params: {
						min:1,
						max:100
					}
				}]
			},
			'.address-state input': {
				value: 'state',
				validations: [{
					type: 'length',
					params: {
						min: 1,
						max:100
					}
				}]
			},
			'.address-zip input': {
				value: 'zip',
				validations: [{
					type: 'length',
					params: {
						min: 1,
						max:100
					}
				}]
			}
		},
		render: function() {
			this.$el.html(templates.address());
			CSViews.addressForm.__super__.render.apply(this, arguments);
			this.applyValidation();
			return this;
		},
		applyValidation: function () {
			if (!this.validation) {
				return;
			}

			var keys = _.keys(this.validation);

			for (var i = 0; i < keys.length; i++) {
				var key = keys[i];
				var binding = this.bindings[key] ? this.bindings[key] + ',' : '';
				var curr = this.validation[key];

				binding += 'validate:{value: ' + curr.value + ', validations: ' + JSON.stringify(curr.validations) + '}';

				this.bindings[key] = binding;
			}

			this.applyBindings();
		}
	});

	CSViews.Thumbnail = BaseView.extend({
		className:"col-xs-6 col-md-3 productModel",
		events: {
			'click .fa-circle': 'pickColor'
		},
		bindings: {
			//'.thumbnail': 'toggle:not(info)',
			//'.panel': 'toggle:info',
			'.fa-check-circle': 'toggle:available'
		},
		initialize: function(options) {
			this.viewModel = new CSModels.BaseModel({
				info: false
			});
			this.model.fetch({
				success: _.bind(this.render, this)
			});
			CSViews.Thumbnail.__super__.initialize.apply(this, arguments);
		},
		render: function() {
			this.$el.html(templates.ProductModelView({product: this.model.toJSON()}));
			CSViews.Thumbnail.__super__.render.apply(this, arguments);
		},
		getDetails: function() {
			this.model.fetch({
				success: _.bind(function(){
					this.viewModel.set('info', true);
					//this.render();
				}, this)
			});
		},
		toggleInfo: function() {
			this.viewModel.set('info', !this.viewModel.get('info'));
		},
		pickColor: function(e) {
			var product = this.model.toJSON();
			var color = this.model.get('colors')[$(e.currentTarget).data('color')];
			this.model.collection.trigger('option_selected', _.extend(product, {
					selectedColor: color,
					color: color.name,
					sizes: color.sizes
				})
			);
			$('a[href="#buy"]').tab('show');
			$('html, body').animate({
			   scrollTop: $('.product-selection').offset().top
			}, 'slow');
		}
	});

	CSViews.ImageArray = BaseView.extend({
		itemView: Thumbnail,
		render: function() {
			this.$el.html(templates.ImageContainer());
			CSViews.ImageArray.__super__.render.apply(this);
		}
	});

	CSViews.CanvasEditor = BaseView.extend({
		events: {
			'change .rmWhite': 'removeWhite'
		},
		className: 'clearfix',
		render: function() {
			this.$el.html(templates.CanvasEditor());
			this.$canvas = this.$el.find('.cseditor');
			this.showPreview();
			CSViews.CanvasEditor.__super__.render.apply(this, arguments);
			return this;
		},

		initialize: function(options) {
			this.viewModel = new CSModels.BaseModel({
				imageData: "",
				artImageStyle: "",
				shirtImage: "",
				shirtImageStyle: "position:static; width:100%",
				containerStyle: "min-height:350px; width:325px; background-color:#ccc; border:1px solid #bbb; border-radius:3px; padding:3px;"
			});
			CSViews.CanvasEditor.__super__.initialize.apply(this, arguments);
		},
		removeWhite: function(e) {
			$check = $(e.currentTarget);
			if (this.sampleImage.filters.length) {
				this.sampleImage.filters.pop();
			} else {
				this.sampleImage.filters.push(new CSFabric.Image.filters.RemoveWhite());
			}
			this.sampleImage.applyFilters(this.fCanvas.renderAll.bind(this.fCanvas));
		},

		showPreview: function() {
			var selectedProd = this.model.toJSON();
			var json = csinit;

			// use product specific options, or default
			var options;
			if (json[selectedProd.productId]) {
				options = json[selectedProd.productId];
			} else {
				options = json.default;	
			}
			this.fCanvas = new CSFabric.Canvas('cseditor');
			this.fCanvas.setHeight(550);
			this.fCanvas.setWidth(540);
			//this.fCanvas.selection = false;

			//select our best option image
			var productImage;
			var productImages = selectedProd.selectedColor.images
			for (var i = 0; i < productImages.length; i ++) {
				if (productImages[i].label === "Front") {
					productImage = productImages[i].url;
				}
			}
			productImage = productImage || selectedProd.productImage;

			//load images in canvas, and apply filters on callback
			CSFabric.Image.fromURL(productImage, _.bind(function(oImg) {
				oImg.set(options.image_style);
				oImg.set('selectable', false);
				this.fCanvas.add(oImg);
			}, this));
			CSFabric.Image.fromURL("/proxy.php?url="+this.model.get('artwork'), _.bind(function(oImg) {
				oImg.set(options.sample_style);
				oImg.filters.push(new CSFabric.Image.filters.RemoveWhite());
				oImg.applyFilters(this.fCanvas.renderAll.bind(this.fCanvas));
				this.sampleImage = oImg;
				this.fCanvas.add(oImg).renderAll();
    			this.fCanvas.setActiveObject(oImg);
				this.trigger('done_loading');
			}, this));
		},
		adjustImage: function(iArray) {asdf
		    var imageData = iArray.data;
		    for (var i = 0; i < imageData.length; i+= 4) {
		    	var dr = Math.abs(255 - imageData[i]);
				var dg = Math.abs(255 - imageData[i+1]);
				var db = Math.abs(255 - imageData[i+2]);
				//imageData[i] = 0;
				//imageData[i+1] = 0;
				//imageData[i+2] = 0;
		       	imageData[i+3] = 255 * ((dr + dg + db)/765);
		    }
		
		    return iArray;            
		},
	});

	CSViews.SelectedProduct = BaseView.extend({
		className: 'selectedProduct',
		bindings: {
			'.size-select': 'options:sizes',
			'.artwork-url input': 'value:artwork',
			'.qty-select': 'options:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]'
		},
		events: {
			'click .close': 'closeButton',
			'click .fa-circle': 'pickColor',
			'change .size-select': 'sizeSelect',
			'change .qty-select': 'qtySelect',
			'change .artwork-url input': 'showPreview'
		},

		initialize: function(options) {
			this.shipping = options.shipping;
			this.default = true;
			CSViews.SelectedProduct.__super__.initialize.apply(this, arguments);
		},
		
		render: function() {
			var productImage;
			var productImages = this.model.get('selectedColor').images;
			for (var i = 0; i < productImages.length; i ++) {
				if (productImages[i].label === "Front") {
					productImage = productImages[i].url;
				}
			}
			productImage = productImage || selectedProd.productImage;
			this.$el.html(templates.SelectedProductView({
				product: this.model.toJSON({
					computed: true
				})
			}));
			this.$addressForm = this.$el.find('.address-form');
			this.$canvas = this.$el.find('.img-preview');
			this.$el.show();
			this.buildSubviews();
			CSViews.SelectedProduct.__super__.render.apply(this, arguments);
			if (this.default) {
				this.$el.find('.qty-select option').filter(function() { 
				    return ($(this).text() == '1');
				}).prop('selected', true);
				this.$el.find('.size-select option').filter(function() { 
				    return ($(this).text() == 'xlg');
				}).prop('selected', true);
				//this.default = false;
			};
		},
		buildSubviews: function() {
				this.addressWiget = new CSViews.addressForm({
					model: this.shipping,
					el: this.$addressForm
				});
				this.addressWiget.render();
				this.canvasEditor = new CSViews.CanvasEditor({
					model: this.model,
					el: this.$canvas
				});
				this.canvasEditor.render();
		},
		showPreview: function() {
			this.canvasEditor.showPreview();
		},
		pickColor: function(e) {
			var product = this.model.toJSON();
			var color = this.model.get('colors')[$(e.currentTarget).data('color')];
			this.collection.trigger('option_selected', _.extend(product, {
					selectedColor: color,
					color: color.name,
					sizes: color.sizes
				})
			);
			$('html, body').animate({
			   scrollTop: $('.product-selection').offset().top
			}, 'slow');
		},
		sizeSelect: function(e) {
			var size = $(e.currentTarget).val();
			this.model.set('size', size);
		},
		qtySelect: function(e) {
			var qty = $(e.currentTarget).val();
			this.model.set('quantity', qty);
		}
	});

	CSViews.ArtworkLanding = BaseView.extend({
		events: {
			'change input': 'progressViews'
		},
		bindings: {
			'input': 'value:artwork'
		},
		render: function() {
			if (this.model.get('artwork').length > 0) {
				this.progressViews();
			} else {
				this.$el.html(templates.ArtworkLanding());
				CSViews.ArtworkLanding.__super__.render.apply(this, arguments);
			}
			return this;
		},
		progressViews: function() {
			this.trigger('artwork_input', this.model.get('artwork'));
		}
	});

	CSViews.Shop = BaseView.extend({
		events: {
			'click .btn-save': 'handleQuote'
		},
		initialize: function(options) {
			this.options = options || {};
			this.categories = this.options.categories;
			this.products = this.options.products;
			this.shipping = this.options.shipping;
			this.prodSelection = this.options.prodSelected;
			this.viewModel = new CSModels.BaseModel({
				img:""
			});
			CSViews.Shop.__super__.initialize.apply(this. arguments);
		},

		render: function() {
			this.$el.html(templates.App());
			this.$catagorySelect = this.$el.find('.category-pills');
			this.$images = this.$el.find('.image-container');
			this.$selection = this.$el.find('.product-selection');
			this.$artwork = this.$el.find('.artwork-landing');
			this.$page2 = this.$el.find('.page2');
			this.$footer = this.$el.find('.footer');
			this.buildSubviews();
			CSViews.Shop.__super__.render.apply(this, arguments);
		},

		buildSubviews: function (){
			this.productSelection = new CSViews.SelectedProduct({
				collection: this.products,
				el: this.$selection,
				model: this.prodSelection,
				shipping: this.shipping
			});
			this.$page2.hide();
			this.categories.on('option_selected', this.optionSelected, this);
			this.products.on('option_selected', this.productSelected, this);
			this.on('sendQuote', this.sendQuote, this);

			this.artworkLanding = new CSViews.ArtworkLanding({
				el: this.$artwork,
				model: this.prodSelection
			});
			this.artworkLanding.on('artwork_input', this.artworkSelected, this);
			this.artworkLanding.render();
		},

		artworkSelected: function(e) {
			var product = new this.products.model({
				id: 'gildan-ultra-cotton-t-shirt'
			});
			product.fetch({
				success: _.bind(function(){
					var colors = product.get('colors');
					var queryColor = csinit.getParameterByName('color') || 'White';
					var chosen = 0;
					for (var i in colors) {
						if (colors[i].name == queryColor) {
							chosen = i;
						}
					}
					this.productSelected(_.extend(product.toJSON(), {
							selectedColor: product.get('colors')[chosen],
							color: queryColor,
							sizes: product.get('colors')[chosen].sizes,
							artwork: e
						})
					);
					$('html, body').animate({
					   scrollTop: $('.product-selection').offset().top
					}, 'slow');
					this.productSelection.canvasEditor.on('done_loading', _.bind(function() {
						this.$page2.show();
						this.$artwork.hide();
					}, this));
					this.productSelection.canvasEditor.showPreview();
				}, this)
			});
		},

		optionSelected: function(e) {
			this.catSelection = this.categories.get(e);
			this.catSelection.fetch({
				success: _.bind(this.gotProducts, this)
			});
		},

		gotProducts: function() {
			this.products.reset(this.catSelection.get('products'));
			this.products.trigger('update');
		},

		productSelected: function(e) {
			this.prodSelection.set(e);
			this.productSelection.render();
			this.prodSelection.on("change", this.calcSavable, this);
			this.shipping.on("parsed", this.calcSavable, this);
		},
		handleQuote: function() {
			var order = _.extend({}, _.pick(this.prodSelection.toJSON(), 
				['productId',
				'color',
				'size',
				'quantity',
				'selectedColor']));
			order = _.extend({}, order, _.pick(this.shipping.toJSON(),
				['address1',
				 'input',
				 'country',
				 'zip',
				 'state', 
				 'city']));
			order.type = 'dtg';
			order.sides = {front:1};
			order.placeOrder = 1;
			order.width = 12;
			//order.artwork = "http://google.com/default.png";
			order.productImage = this.prodSelection.get('image').url;
			order.fromUrl = true;

			this.quote = _.extend({}, order);
			var handler = StripeCheckout.configure({
				key: 'pk_live_P9WcuSbQ9Z6ChFCDEnFZ6dJH',
				image: order.artwork,
				closed: _.bind(function(e) {
				}, this),
				token: _.bind(function(token) {
					if (token.email) {
						$.ajax({
							type: 'POST',
							data: _.extend(order, token),
							url: "http://shirts-api.openrobot.net",
							success: _.bind(function(data){
								console.log(data);
								
							}, this),
							error: _.bind(function(data){
								alert("Error, please contact xanderjanz@gmail.com");
							}, this)
						});
					}
				}, this)
			});
			// Open Checkout with further options
			handler.open({
				name: 'ClickShirt',
				description: order.quantity + " " +
							 order.color + 
							 ' printed product(s). ($20.00 ea)',
				amount: 2000 * order.quantity
			});
		},
		calcSavable: function(e) {
			var shipping = this.shipping.toJSON();
			var product = this.prodSelection.toJSON()
			var productTests = 
				['productId',
				'color',
				'size',
				'quantity',
				'selectedColor'];
			var shippingTests = 
				['address1',
				 'country',
				 'zip',
				 'state', 
				 'city'] 
			var valid = true;
			for (var test in productTests) {
				valid = valid && !!(product[productTests[test]]);
			}
			for (var test in shippingTests) {
				valid = valid && !!(shipping[shippingTests[test]]);
			}
			if (valid) {
				this.$el.find('.btn-save').removeClass("disabled");
			} else {
				this.$el.find('.btn-save').addClass("disabled");
			}
		},
		sendQuote: function(link) {
			var link = link.data;
			if (link.srcUrl) {
				this.quote.Artwork = link.srcUrl;
				$.ajax({
					type: 'POST',
					data: this.quote,
					url: postApi,
					success: _.bind(function(data){
						alert(JSON.stringify(data));
					}, this)
				});
			}
		}
	});
return CSViews;
});