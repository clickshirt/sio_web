define('csapp', [
	'jquery',
	'csinit',
	'epoxy',
	'backbone',
	'csmodels',
	'cscollections',
	'csviews/Shop'], function($, csinit, Epoxy, Backbone, CSModels, CSCollections, Shop) {
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-37375670-2']);
	_gaq.push(['_trackPageview']);

	(function() {	
	  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	  ga.src = 'https://ssl.google-analytics.com/ga.js';
	  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	(function() {
	var catagoryFull = new CSCollections.Catagories();
	var productFull = new CSCollections.Products();
	var ourShipping = new CSModels.Address();
	var prodSelection = new CSModels.ProductOrder({
		artwork: csinit.getParameterByName('url')
	});
	var ourDesign = new CSModels.Design();
	var shop = window.shop = new Shop({
		el: $('.app'),
		categories: catagoryFull,
		products: productFull,
		shipping: ourShipping,
		design: ourDesign,
		prodSelected: prodSelection
	});


	var catIds = {
		//'mugs':'Mugs',
		//'phone-cases':'Phone Cases',
		//'long-sleeve-shirts': 'Long Sleeve Shirts',
		'short-sleeve-shirts': 'Short Sleeve Shirts'
	};
	var catagories = [];
	for (var id in catIds) {
		var cat = new CSModels.Category({
			categoryId: id,
			name: catIds[id]
		});
		cat.fetch();
		catagories.push(cat);
	}
	var shortSleeve = new CSModels.Category({categoryId:'long-sleeve-shirts'});
	var longSleeve = new CSModels.Category({categoryId: 'short-sleeve-shirts'});
	var mugsCategory = new CSModels.Category({categoryId: 'mugs'});
	var caseCategory = new CSModels.Category({categoryId: 'phone-cases'});
	catagoryFull.add(catagories);
	//Pre-select Ladies Short Sleve
	window.shop.optionSelected('short-sleeve-shirts');
	$('a[value="short-sleeve-shirts"]').parent().addClass('active');
	shop.render();
	})();
});