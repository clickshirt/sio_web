// Set the require.js configuration for your application.
require.config({

	baseUrl: "/scripts",

	paths: {
		//bower packages
		jquery: "../vendor/bower/jquery/jquery",
		bootstrap: '../vendor/bower/bootstrap/dist/js/bootstrap',
		underscore: '../vendor/bower/underscore/underscore',
		backbone: '../vendor/bower/backbone/backbone',

		handlebars: "../vendor/bower/handlebars/handlebars",
		hbsHelpers: '../vendor/bower/handlebars-helpers/helpers',
		epoxy: "../vendor/bower/backbone.epoxy/backbone.epoxy",
		fabric: "../vendor/bower/fabric/dist/fabric.require",
		stripe: "../vendor/bower/stripe/index",

		'canvas-to-blob': '../vendor/bower/blueimp-canvas-to-blob/js/canvas-to-blob',
		'load-image-exif': '../vendor/bower/blueimp-load-image/js/load-image-exif',
		'load-image-ios': '../vendor/bower/blueimp-load-image/js/load-image-ios',
		'load-image-meta': '../vendor/bower/blueimp-load-image/js/load-image-meta',
		'load-image-orientation': '../vendor/bower/blueimp-load-image/js/load-image-orientation',
		'load-image': '../vendor/bower/blueimp-load-image/js/load-image',

		subviews: '../vendor/backbone-plugins/backbone.subviews',
	},

	shim: {
		underscore: {
			exports: '_'
		},
		// momentjs: {
		// 	exports: "moment"
		// },
		backbone: {
			deps: [
				'jquery',
				'underscore'
			],
			exports: 'Backbone'
		},
		handlebars: {
			exports: 'Handlebars'
		},
		bootstrap: {
			deps: ['jquery'],
			exports: "$.fn.popover"
		},
		fabric: {
			deps: ['epoxy'],
			exports: 'fabric'
		},
		hbsHelpers: {
			deps: ['handlebars']
		},
		tcHelpers: {
			deps: ['handlebars']
		},
		facebook: {
			exports: "FB"
		}
	}
});

require([
	'csapp'
]);

