<?php
// PHP Proxy
// Responds to both HTTP GET and POST requests
//
// Author: Abdul Qabiz
// March 31st, 2006
//

require_once 'google/appengine/api/cloud_storage/CloudStorageTools.php';
use google\appengine\api\cloud_storage\CloudStorageTools;

// Get the url of to be proxied
// Is it a POST or a GET?
$url = ($_POST['url']) ? $_POST['url'] : $_GET['url'];
$headers = ($_POST['headers']) ? $_POST['headers'] : $_GET['headers'];
$mimeType =($_POST['mimeType']) ? $_POST['mimeType'] : $_GET['mimeType'];
$handle = fopen($url, 'r');
$response = stream_get_contents($handle);


if ($mimeType != "")
{
	// The web service returns XML. Set the Content-Type appropriately
	header("Content-Type: ".$mimeType);
	header("Access-Control-Allow-Origin: *");
} else {
	header("Content-Type: image/jpeg");
	header("Access-Control-Allow-Origin: *");
}
$gs_url = 'gs://click_shirt/'.pathinfo(parse_url($url, PHP_URL_PATH))['basename'];
file_put_contents($gs_url, $response);
$ret_url = CloudStorageTools::getPublicUrl($gs_url, false);
echo $response;

fclose($handle);

?>